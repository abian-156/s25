//Creat a standard server app using Node.Js.

// get the http module using the require() directive.
//repackage the module on a new variable.
	const http = require('http');

	const host = 4000;
//creat the server and place inside a new variable
//to give it an identifier.
	
	//Arguments =>
	let server = http.createServer((req, res) => {
		res.end('Welcome to the App');
	})

//Assign a designated port for that will serve the project.by binding the connection with the desired port.
	server.listen(host);

//Includde a response that will be displayed in the console
// to verify the connection that was establish.

	console.log(`Listening on port: ${host}`);
